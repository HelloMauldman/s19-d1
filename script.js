console.log("JS ES6 Update");

//1. Exponent Operators
const firstNum = 8 ** 2;
console.log(firstNum);

const secondNum = Math.pow(8,2);
console.log(secondNum);

//2. Template literals
let name = "John";
let message = "Hello " + name + "! Welcome to programming!";

console.log("message without template literals: " + message);

let message1 = `Hello ${name}! Welcome to programming!`;

console.log(`message with template literals: ${message1}`);


// multi-line
const anotherMessage = `

${name} attended a Math Competition. He won it by solving the problem 8 ** 2 with the solution of ${firstNum}.`;
console.log(anotherMessage);

const interstRate = .1;
const principal = 1000;
console.log(`The interest on your savings account is ${principal * interstRate}.`);

//3. Array Destructuring 

const fullName = ["Juan", "Dela", "Cruz"]
//before
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);
//destructuring 
const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);
console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`)


//4. Object Destructuring
const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
};

//before 
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`);
// destructuring

const { givenName, maidenName, familyName } = person;
console.log(givenName);
console.log(maidenName);
console.log(lastName);
console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again.`);

// 5. Arrow Function

//before

const hello = function(){
	console.log("Hellow world!");
}
//using arrow function
const helloAgain = () => {
	console.log("Hellow world!");
}

function hello2(){
	console.log("Hello World!")
}

hello();
hello2();

//Arrow functions with loops
const students = ["John", "Jane", "Judy"];

//before
students.forEach(function(student){
	console.log(`${student} is a student.`)
})

// using arrow functions on loops
students.forEach((student) =>{
	console.log(`${student} is a student.`)
})

// 6. implicit return statement

const add = (x, y) => x + y;
let total = add(1, 2);
console.log(total);

//7. Default Functino Argument Value
const greet = (name = "User") =>{
	return `Good morning, ${name}!`;
}
console.log(greet());
console.log(greet("John"));

//8.Class

class Car {
	constructor(brand, name, year){
		this.brand = year;
		this.year = year;
		this. name = name;
	}
}

const myCar = new Car();
console.log(myCar)

myCar.brand = "Ford";
myCar.name = "Raptor Raptop";
myCar.year = 2021;

console.log(myCar)

const myNewCar = new Car("Lambo", "Handsome", 2021);

console.log(myNewCar);